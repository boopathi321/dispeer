package com.dispeer.app.util;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;

/**
 * Created by My Windows on 9/23/2017.
 */

public class StaticUtils {

    public static Bitmap clipit(Bitmap bitmapimg,int direct) {

        //1 = direction right
        //0 = direction left

        Bitmap output = Bitmap.createBitmap(bitmapimg.getWidth(),
                bitmapimg.getHeight(), Bitmap.Config.ARGB_8888);
        final Paint paint = new Paint();
        ColorFilter filter = new LightingColorFilter(Color.BLUE, 1);
        paint.setColorFilter(filter);


        final int color = Color.WHITE;

        final Rect rect = new Rect(0, 0, bitmapimg.getWidth(),
                bitmapimg.getHeight());
        Canvas canvas = new Canvas(output);

//        paint.setAntiAlias(true);
//        canvas.drawARGB(0, 0, 0, 0);
//        paint.setColor(color);

        if(direct == 0) {
            canvas.drawRect(0, 0, bitmapimg.getWidth()-15, bitmapimg.getHeight(), paint);
            Path path = new Path();
            path.moveTo(bitmapimg.getWidth()-15, 10);
            path.lineTo(bitmapimg.getWidth(), 20);
            path.lineTo(bitmapimg.getWidth()-15, 30);
            path.lineTo(bitmapimg.getWidth()-15, 10);
            canvas.drawPath(path,paint);
        }
        if(direct == 1) {
            canvas.drawRect(15, 0, bitmapimg.getWidth(), bitmapimg.getHeight(), paint);
            Path path = new Path();
            path.moveTo(15, 10);
            path.lineTo(0, 20);
            path.lineTo(15, 30);
            path.lineTo(15, 10);
            canvas.drawPath(path,paint);
        }


        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmapimg, rect, rect, paint);
        canvas.drawText("Testing...", 10, 10, paint);
        return output;

    }
}
