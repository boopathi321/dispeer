package com.dispeer.app.activity.util;

public class User
{
    private String timestamputc;

    private String uservalididtycode;

    private String namereal;

    private String nickname;

    private String autodeletechat;

    private String autodeletecontact;

    private String messagenotificationstate;

    private String uservalididtydesc;

    private String oneSignalId;

    private String chatpermision;

    private String updatedAt;

    private String timestampserver;

    private String timezone;

    private String messagenotification;

    private String objectId;

    private String createdAt;

    private String groupenotificationstate;

    private String name;

    private String userStatus;

    private String groupnotification;

    private String lastactivity;

    private String userdeletestatus;

    public User(){

    }

    public User(String timestamputc, String uservalididtycode, String namereal, String nickname, String autodeletechat, String autodeletecontact, String messagenotificationstate, String uservalididtydesc, String oneSignalId, String chatpermision, String updatedAt, String timestampserver, String timezone, String messagenotification, String objectId, String createdAt, String groupenotificationstate, String name, String userStatus, String groupnotification, String lastactivity, String userdeletestatus) {
        this.timestamputc = timestamputc;
        this.uservalididtycode = uservalididtycode;
        this.namereal = namereal;
        this.nickname = nickname;
        this.autodeletechat = autodeletechat;
        this.autodeletecontact = autodeletecontact;
        this.messagenotificationstate = messagenotificationstate;
        this.uservalididtydesc = uservalididtydesc;
        this.oneSignalId = oneSignalId;
        this.chatpermision = chatpermision;
        this.updatedAt = updatedAt;
        this.timestampserver = timestampserver;
        this.timezone = timezone;
        this.messagenotification = messagenotification;
        this.objectId = objectId;
        this.createdAt = createdAt;
        this.groupenotificationstate = groupenotificationstate;
        this.name = name;
        this.userStatus = userStatus;
        this.groupnotification = groupnotification;
        this.lastactivity = lastactivity;
        this.userdeletestatus = userdeletestatus;
    }

    public String getTimestamputc ()
    {
        return timestamputc;
    }

    public void setTimestamputc (String timestamputc)
    {
        this.timestamputc = timestamputc;
    }

    public String getUservalididtycode ()
    {
        return uservalididtycode;
    }

    public void setUservalididtycode (String uservalididtycode)
    {
        this.uservalididtycode = uservalididtycode;
    }

    public String getNamereal ()
    {
        return namereal;
    }

    public void setNamereal (String namereal)
    {
        this.namereal = namereal;
    }

    public String getNickname ()
    {
        return nickname;
    }

    public void setNickname (String nickname)
    {
        this.nickname = nickname;
    }

    public String getAutodeletechat ()
    {
        return autodeletechat;
    }

    public void setAutodeletechat (String autodeletechat)
    {
        this.autodeletechat = autodeletechat;
    }

    public String getAutodeletecontact ()
    {
        return autodeletecontact;
    }

    public void setAutodeletecontact (String autodeletecontact)
    {
        this.autodeletecontact = autodeletecontact;
    }

    public String getMessagenotificationstate ()
    {
        return messagenotificationstate;
    }

    public void setMessagenotificationstate (String messagenotificationstate)
    {
        this.messagenotificationstate = messagenotificationstate;
    }

    public String getUservalididtydesc ()
    {
        return uservalididtydesc;
    }

    public void setUservalididtydesc (String uservalididtydesc)
    {
        this.uservalididtydesc = uservalididtydesc;
    }

    public String getOneSignalId ()
    {
        return oneSignalId;
    }

    public void setOneSignalId (String oneSignalId)
    {
        this.oneSignalId = oneSignalId;
    }

    public String getChatpermision ()
    {
        return chatpermision;
    }

    public void setChatpermision (String chatpermision)
    {
        this.chatpermision = chatpermision;
    }

    public String getUpdatedAt ()
    {
        return updatedAt;
    }

    public void setUpdatedAt (String updatedAt)
    {
        this.updatedAt = updatedAt;
    }

    public String getTimestampserver ()
    {
        return timestampserver;
    }

    public void setTimestampserver (String timestampserver)
    {
        this.timestampserver = timestampserver;
    }

    public String getTimezone ()
    {
        return timezone;
    }

    public void setTimezone (String timezone)
    {
        this.timezone = timezone;
    }

    public String getMessagenotification ()
    {
        return messagenotification;
    }

    public void setMessagenotification (String messagenotification)
    {
        this.messagenotification = messagenotification;
    }

    public String getObjectId ()
    {
        return objectId;
    }

    public void setObjectId (String objectId)
    {
        this.objectId = objectId;
    }

    public String getCreatedAt ()
    {
        return createdAt;
    }

    public void setCreatedAt (String createdAt)
    {
        this.createdAt = createdAt;
    }

    public String getGroupenotificationstate ()
    {
        return groupenotificationstate;
    }

    public void setGroupenotificationstate (String groupenotificationstate)
    {
        this.groupenotificationstate = groupenotificationstate;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getUserStatus ()
    {
        return userStatus;
    }

    public void setUserStatus (String userStatus)
    {
        this.userStatus = userStatus;
    }

    public String getGroupnotification ()
    {
        return groupnotification;
    }

    public void setGroupnotification (String groupnotification)
    {
        this.groupnotification = groupnotification;
    }

    public String getLastactivity ()
    {
        return lastactivity;
    }

    public void setLastactivity (String lastactivity)
    {
        this.lastactivity = lastactivity;
    }

    public String getUserdeletestatus ()
    {
        return userdeletestatus;
    }

    public void setUserdeletestatus (String userdeletestatus)
    {
        this.userdeletestatus = userdeletestatus;
    }
}
