package com.dispeer.app.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.dispeer.app.activity.util.Message;
import com.dispeer.app.adapter.ChatDetailAdapter;
import com.dispeer.app.fragment.AttachmentFragment;
import com.dispeer.app.util.RandomString;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.vweeter.dispeer.R;

import java.io.File;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Map;

/**
 * Created by My Windows on 8/22/2017.
 */

public class ChatDetailActivity extends BaseActivity implements View.OnClickListener {
    private Toolbar mToolbar;
    private RecyclerView recyclerView;
    private ArrayList msgList;
    private TextView send;
    private EditText msgEditText;
    private ChatDetailAdapter mChatDetailAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private FirebaseDatabase mDatabase;
    private DatabaseReference mDatabaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat_detail);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        msgList = new ArrayList();
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        initControl();
    }

    private void initControl() {
        mDatabase = FirebaseDatabase.getInstance();
        mDatabaseReference = mDatabase.getReference("Message");
        recyclerView = (RecyclerView) findViewById(R.id.chatdetail);
        ImageView attachment = (ImageView) findViewById(R.id.attachment);
        attachment.setOnClickListener(this);
        send = (TextView) findViewById(R.id.send);
        msgEditText = (EditText) findViewById(R.id.msgEditText);
        send.setOnClickListener(this);
        mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        mChatDetailAdapter = new ChatDetailAdapter(this, msgList);
        recyclerView.setAdapter(mChatDetailAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.send) {
            if (!"".equals(msgEditText.getText().toString())) {
                sendMessage(msgEditText.getText().toString(), "text");
            }
        } else if (v.getId() == R.id.attachment) {
            AttachmentFragment mAttachmentFragment = new AttachmentFragment();
            mAttachmentFragment.show(getSupportFragmentManager(), "");
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == 203) {
            if (resultCode == 202) {
                TransferObserver observer = DispeerApplication.getTransferUtility().upload(
                        "dispeerapp/android/audio/S3_BUCKET_AUDIOS",           // The S3 bucket to upload to
                        "one.mp3", new File(data.getStringExtra("LOCATION_URL")));
                observer.setTransferListener(new TransferListener() {
                    @Override
                    public void onStateChanged(int id, TransferState state) {
                        System.out.println("1");
                    }

                    @Override
                    public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                        System.out.println("2");
                    }

                    @Override
                    public void onError(int id, Exception ex) {
                        System.out.println("3");
                    }
                });
            }else if(resultCode == Activity.RESULT_OK){
                System.out.println("");
            }
//        }
    }

    private void sendMessage(String msg , String type){
        Message mMessage = new Message();
        RandomString tickets = new RandomString(28, new SecureRandom(), "");
        String groupId = tickets.nextString();
        String objectId = tickets.nextString();
        mMessage.setAudio("");
        mMessage.setAudio_md5("");
        mMessage.setAudio_duration("0");
        mMessage.setChattype("private");
        mMessage.setCreatedAt(String.valueOf(System.currentTimeMillis()));
        mMessage.setGroupId(groupId);
        mMessage.setIsDeleted(false);
        mMessage.setLatitude("0");
        mMessage.setLongitude("0");
        mMessage.setObjectId(objectId);
        mMessage.setPicture("");
        mMessage.setPicture_height("0");
        mMessage.setPicture_md5("");
        mMessage.setPicture_width("0");
        mMessage.setSenderId("DfwcZUKxOBdDLEsG8RB2qZjWPEf2");
        mMessage.setSenderName("boopathi");
        mMessage.setSenderRealName("mohai");
        mMessage.setStatus("");
        mMessage.setSubtext("");
        mMessage.setSubtype("");
        mMessage.setText(msg);
        mMessage.setType(type);
        mMessage.setUpdatedAt(String.valueOf(System.currentTimeMillis()));
        mMessage.setVideo("");
        mMessage.setVideo_duration("0");
        mMessage.setVideo_md5("");
        mDatabaseReference.child(groupId).child(objectId).setValue(mMessage);
        mDatabase.getReference("Message").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                msgList.add(msgEditText.getText().toString());
                mChatDetailAdapter.notifyDataSetChanged();
                mLayoutManager.scrollToPosition(msgList.size() - 1);
                msgEditText.setText("");
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }
}
