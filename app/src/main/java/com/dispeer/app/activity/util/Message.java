package com.dispeer.app.activity.util;

/**
 * Created by My Windows on 9/24/2017.
 */

public class Message {
    private String subtext;

    private String text;

    private String video_md5;

    private String audio;

    private String status;

    private String senderRealName;

    private String chattype;

    private String picture_height;

    private String picture_width;

    private String senderId;

    private String type;

    private String audio_md5;

    private boolean isDeleted;

    private String audio_duration;

    private String updatedAt;

    private String picture;

    private String groupId;

    private String senderName;

    private String picture_md5;

    private String createdAt;

    private String objectId;

    private String subtype;

    private String longitude;

    private String latitude;

    private String video_duration;

    private String video;

    public String getSubtext ()
    {
        return subtext;
    }

    public void setSubtext (String subtext)
    {
        this.subtext = subtext;
    }

    public String getText ()
    {
        return text;
    }

    public void setText (String text)
    {
        this.text = text;
    }

    public String getVideo_md5 ()
    {
        return video_md5;
    }

    public void setVideo_md5 (String video_md5)
    {
        this.video_md5 = video_md5;
    }

    public String getAudio ()
    {
        return audio;
    }

    public void setAudio (String audio)
    {
        this.audio = audio;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public String getSenderRealName ()
    {
        return senderRealName;
    }

    public void setSenderRealName (String senderRealName)
    {
        this.senderRealName = senderRealName;
    }

    public String getChattype ()
    {
        return chattype;
    }

    public void setChattype (String chattype)
    {
        this.chattype = chattype;
    }

    public String getPicture_height ()
    {
        return picture_height;
    }

    public void setPicture_height (String picture_height)
    {
        this.picture_height = picture_height;
    }

    public String getPicture_width ()
    {
        return picture_width;
    }

    public void setPicture_width (String picture_width)
    {
        this.picture_width = picture_width;
    }

    public String getSenderId ()
    {
        return senderId;
    }

    public void setSenderId (String senderId)
    {
        this.senderId = senderId;
    }

    public String getType ()
    {
        return type;
    }

    public void setType (String type)
    {
        this.type = type;
    }

    public String getAudio_md5 ()
    {
        return audio_md5;
    }

    public void setAudio_md5 (String audio_md5)
    {
        this.audio_md5 = audio_md5;
    }

    public boolean getIsDeleted ()
    {
        return isDeleted;
    }

    public void setIsDeleted (boolean isDeleted)
    {
        this.isDeleted = isDeleted;
    }

    public String getAudio_duration ()
    {
        return audio_duration;
    }

    public void setAudio_duration (String audio_duration)
    {
        this.audio_duration = audio_duration;
    }

    public String getUpdatedAt ()
    {
        return updatedAt;
    }

    public void setUpdatedAt (String updatedAt)
    {
        this.updatedAt = updatedAt;
    }

    public String getPicture ()
    {
        return picture;
    }

    public void setPicture (String picture)
    {
        this.picture = picture;
    }

    public String getGroupId ()
    {
        return groupId;
    }

    public void setGroupId (String groupId)
    {
        this.groupId = groupId;
    }

    public String getSenderName ()
    {
        return senderName;
    }

    public void setSenderName (String senderName)
    {
        this.senderName = senderName;
    }

    public String getPicture_md5 ()
    {
        return picture_md5;
    }

    public void setPicture_md5 (String picture_md5)
    {
        this.picture_md5 = picture_md5;
    }

    public String getCreatedAt ()
    {
        return createdAt;
    }

    public void setCreatedAt (String createdAt)
    {
        this.createdAt = createdAt;
    }

    public String getObjectId ()
    {
        return objectId;
    }

    public void setObjectId (String objectId)
    {
        this.objectId = objectId;
    }

    public String getSubtype ()
    {
        return subtype;
    }

    public void setSubtype (String subtype)
    {
        this.subtype = subtype;
    }

    public String getLongitude ()
    {
        return longitude;
    }

    public void setLongitude (String longitude)
    {
        this.longitude = longitude;
    }

    public String getLatitude ()
    {
        return latitude;
    }

    public void setLatitude (String latitude)
    {
        this.latitude = latitude;
    }

    public String getVideo_duration ()
    {
        return video_duration;
    }

    public void setVideo_duration (String video_duration)
    {
        this.video_duration = video_duration;
    }

    public String getVideo ()
    {
        return video;
    }

    public void setVideo (String video)
    {
        this.video = video;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [subtext = "+subtext+", text = "+text+", video_md5 = "+video_md5+", audio = "+audio+", status = "+status+", senderRealName = "+senderRealName+", chattype = "+chattype+", picture_height = "+picture_height+", picture_width = "+picture_width+", senderId = "+senderId+", type = "+type+", audio_md5 = "+audio_md5+", isDeleted = "+isDeleted+", audio_duration = "+audio_duration+", updatedAt = "+updatedAt+", picture = "+picture+", groupId = "+groupId+", senderName = "+senderName+", picture_md5 = "+picture_md5+", createdAt = "+createdAt+", objectId = "+objectId+", subtype = "+subtype+", longitude = "+longitude+", latitude = "+latitude+", video_duration = "+video_duration+", video = "+video+"]";
    }
}
