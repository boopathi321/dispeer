package com.dispeer.app.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.dispeer.app.component.WaveFormView;
import com.vweeter.dispeer.R;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by My Windows on 8/28/2017.
 */

public class RecordingActivity extends BaseActivity implements View.OnClickListener {

    private TextView mTimerTextView;
    @BindView(R.id.cancel) TextView cancel;
    @BindView(R.id.done) TextView done;
    @BindView(R.id.recorder) TextView recorder;
    @BindView(R.id.start) ImageView start;
    @BindView(R.id.pause) ImageView pause;
    @BindView(R.id.play) ImageView play;
    @BindView(R.id.stop) ImageView stop;
    @BindView(R.id.delete) ImageView delete;
    private WaveFormView waveForm;
    private Button mCancelButton;
    private Button mStopButton;
    private Runnable mRunnable;
    private boolean permissionToRecordAccepted = false;
    private boolean permissionToWriteAccepted = false;
    private String [] permissions = {"android.permission.RECORD_AUDIO", "android.permission.WRITE_EXTERNAL_STORAGE"};

    private MediaRecorder mRecorder;
    private long mStartTime = 0;

    private int[] amplitudes = new int[100];
    private int i = 0;

    private Handler mHandler = new Handler();
    int count =0;
    private Runnable mTickExecutor = new Runnable() {
        @Override
        public void run() {
            count++;
            mTimerTextView.setText("00"+":"+"0"+count);
            if(count==5){
//                stopRecording(true);
                count =0;
            }else {
                mHandler.postDelayed(mTickExecutor, 1000);
            }
        }
    };
    private File mOutputFile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recording_layout);
        ButterKnife.bind(this);
        this.mTimerTextView = (TextView) this.findViewById(R.id.timer);
        this.waveForm = (WaveFormView) this.findViewById(R.id.waveForm);
        this.mCancelButton = (Button) this.findViewById(R.id.cancel_button);
        start.setOnClickListener(this);
        this.mCancelButton.setOnClickListener(this);
        this.mStopButton = (Button) this.findViewById(R.id.share_button);
        this.mStopButton.setOnClickListener(this);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        waveForm.updateStripColor(getResources().getColor(R.color.audioStrip));
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("Voice Recorder", "output: " + getOutputFile());
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    0);

        }else {
//            startRecording();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mRecorder != null) {
            stopRecording(false);
        }
    }
    private void startRecording() {
        mRecorder = new MediaRecorder();
        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.HE_AAC);
            mRecorder.setAudioEncodingBitRate(48000);
        } else {
            mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
            mRecorder.setAudioEncodingBitRate(64000);
        }
        mRecorder.setAudioSamplingRate(16000);
        mOutputFile = getOutputFile();
        try {
            mOutputFile.getParentFile().createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        File output=
                new File(
                        Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),
                        "recording.mp3");
        mRecorder.setOutputFile(output.getAbsolutePath());

        try {
            mRecorder.prepare();
            mRecorder.start();
            waveForm.updateAmplitude(mRecorder.getMaxAmplitude(), true);
//            final Handler mHandler = new Handler();
//            mRunnable = new Runnable() {
//                @Override
//                public void run() {
//                    waveForm.updateAmplitude(mRecorder.getMaxAmplitude(), true);
//                    mHandler.postDelayed(mRunnable, 500);
//                }
//            };
//            mHandler.postDelayed(mRunnable, 200);
            mStartTime = SystemClock.elapsedRealtime();
            mHandler.postDelayed(mTickExecutor, 100);
            Log.d("Voice Recorder","started recording to "+mOutputFile.getAbsolutePath());
        } catch (IOException e) {
            Log.e("Voice Recorder", "prepare() failed "+e.getMessage());
        }
    }

    protected  void stopRecording(boolean saveFile) {
        mHandler.removeCallbacks(mTickExecutor);
        mRecorder.stop();
        mRecorder.release();
        mRecorder = null;
        mStartTime = 0;
        if (!saveFile && mOutputFile != null) {
            mOutputFile.delete();
        }
        File output=
                new File(
                        Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),
                        "recording.mp3");
        Intent mIntent = new Intent();
        mIntent.putExtra("LOCATION_URL", output.getAbsolutePath());
        setResult(202, mIntent);
        finish();
    }

    private File getOutputFile() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HHmmssSSS", Locale.US);
        return new File(Environment.getExternalStorageDirectory().getAbsolutePath().toString()
                + "/Voice Recorder/RECORDING_"
                + dateFormat.format(new Date())
                + ".m4a");
    }

    private void tick() {
        long time = (mStartTime < 0) ? 0 : (SystemClock.elapsedRealtime() - mStartTime);
        int minutes = (int) (time / 60000);
        int seconds = (int) (time / 1000) % 60;
        int milliseconds = (int) (time / 100) % 10;
        mTimerTextView.setText("0"+minutes+":"+(seconds < 10 ? "0"+seconds : seconds));
        if(seconds == 5)
            stopRecording(true);
        if (mRecorder != null) {
            amplitudes[i] = mRecorder.getMaxAmplitude();
            //Log.d("Voice Recorder","amplitude: "+(amplitudes[i] * 100 / 32767));
            if (i >= amplitudes.length -1) {
                i = 0;
            } else {
                ++i;
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cancel_button:
                stopRecording(false);
                setResult(RESULT_CANCELED);
                finish();
                break;
            case R.id.share_button:
                stopRecording(true);
                Uri uri = Uri.parse("file://" + mOutputFile.getAbsolutePath());
                Intent scanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                scanIntent.setData(uri);
                sendBroadcast(scanIntent);
                setResult(Activity.RESULT_OK, new Intent().setData(uri));
                finish();
                break;
            case R.id.start:
                play.setVisibility(View.GONE);
                stop.setVisibility(View.VISIBLE);
                start.setVisibility(View.GONE);
                pause.setVisibility(View.VISIBLE);
                recorder.setVisibility(View.GONE);
                mTimerTextView.setVisibility(View.VISIBLE);
                cancel.setTextColor(getResources().getColor(R.color.audioRedStrip));
                waveForm.updateStripColor(getResources().getColor(R.color.audioRedStrip));
                startRecording();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case 200:
                permissionToRecordAccepted  = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                permissionToWriteAccepted  = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                startRecording();
                break;
        }
        if (!permissionToRecordAccepted ) finish();
        if (!permissionToWriteAccepted ) finish();

    }
}
