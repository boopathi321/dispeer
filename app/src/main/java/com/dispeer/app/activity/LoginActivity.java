package com.dispeer.app.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Toast;

import com.dispeer.app.activity.util.DispeerRandomGen;
import com.dispeer.app.activity.util.User;
import com.dispeer.app.component.BottomSheet;
import com.dispeer.app.util.RandomString;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.vweeter.dispeer.R;

import java.security.SecureRandom;
import java.util.Map;
import java.util.TimeZone;


public class LoginActivity extends BaseActivity implements View.OnClickListener, BottomSheet.BottomSheetListener {
    private Button randomUsername, loginBtn;
    private EditText randomUserKey, nickname;
    private FirebaseDatabase mDatabase;
    private DatabaseReference mDatabaseReference;
    private LinearLayout progress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_page);
        randomUsername=(Button) findViewById(R.id.randomBtn);
        progress=(LinearLayout) findViewById(R.id.progress);
        loginBtn=(Button) findViewById(R.id.loginBtn);
        loginBtn.setOnClickListener(this);
        randomUserKey=(EditText) findViewById(R.id.randomname);
        nickname=(EditText) findViewById(R.id.nickname);
        randomUsername.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String randomName=DispeerRandomGen.generateRandomString();
                randomUserKey.setText(randomName);
            }
        });
//        initiatePopupWindow();
        //showActionSheet();


    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mDatabase = FirebaseDatabase.getInstance();
        mDatabaseReference = mDatabase.getReference("User");

    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }


    @Override
    public void onClick(View v) {
        if(v.getId() == loginBtn.getId()){
            if(randomUserKey != null && !"".equals(randomUserKey.getText().toString())) {
                isUserNameAvailable(randomUserKey.getText().toString().toLowerCase());
                progress.setVisibility(View.VISIBLE);
            }else{
                Toast.makeText(LoginActivity.this, "Please enter username", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void registerUserData(String userValidityCode){
        User user = new User();
        user.setChatpermision("0");
        user.setAutodeletechat("0");
        user.setAutodeletecontact("0");
        user.setLastactivity(String.valueOf(System.currentTimeMillis()));
        user.setTimestamputc(String.valueOf(System.currentTimeMillis()));
        user.setTimestampserver(String.valueOf(System.currentTimeMillis()));
        user.setTimezone(TimeZone.getDefault().toString());
        user.setUservalididtycode(userValidityCode);
        user.setUservalididtydesc("");
        user.setUserdeletestatus("0");
        user.setName(randomUserKey.getText().toString().toLowerCase());
        user.setNamereal(randomUserKey.getText().toString().toLowerCase());
        user.setNickname(nickname.getText().toString());
        user.setGroupenotificationstate("1");
        user.setGroupnotification("Note");
        user.setMessagenotificationstate("1");
        user.setMessagenotification("Note");
        user.setOneSignalId("");
        RandomString tickets = new RandomString(28, new SecureRandom(), "");
        mDatabaseReference.child(tickets.nextString()).setValue(user);
        mDatabase.getReference("User").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Map<String, Object> map = (Map<String, Object>) dataSnapshot.getValue();
                progress.setVisibility(View.GONE);
                startActivity(new Intent(LoginActivity.this, DashBoardActivity.class));
                finish();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(LoginActivity.this, "Login Failed", Toast.LENGTH_LONG).show();
                progress.setVisibility(View.GONE);
            }
        });
    }

    private void isUserNameAvailable(String userName){
        DatabaseReference mTest = FirebaseDatabase.getInstance().getReference();
        Query query = mTest.child("User").orderByChild("name").equalTo(userName);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getChildrenCount() > 0) {
                    Toast.makeText(LoginActivity.this, "Username already exist", Toast.LENGTH_LONG).show();
                }else{
                    BottomSheetDialogFragment bottomSheetDialogFragment = new BottomSheet();
                    bottomSheetDialogFragment.show(getSupportFragmentManager(), bottomSheetDialogFragment.getTag());
//                    initiatePopupWindow();
                }
                progress.setVisibility(View.GONE);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                progress.setVisibility(View.GONE);
            }
        });
    }


    @Override
    public void registerUser(String code) {
        registerUserData(code);
        progress.setVisibility(View.VISIBLE);
    }
}
