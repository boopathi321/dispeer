package com.dispeer.app.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.dispeer.app.activity.RecordingActivity;
import com.vweeter.dispeer.R;

/**
 * Created by My Windows on 8/23/2017.
 */

public class AttachmentFragment extends DialogFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.attachment_layout, null);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        initControl(view);
        return view;
    }

    private void initControl(View view){
        LinearLayout audio_layout = (LinearLayout) view.findViewById(R.id.audio_layout);
        LinearLayout picture_layout = (LinearLayout) view.findViewById(R.id.picture_layout);
        LinearLayout videoo_layout = (LinearLayout) view.findViewById(R.id.videoo_layout);
        audio_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                startActivityForResult(new Intent(getActivity(), RecordingActivity.class), 203);
            }
        });
        picture_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                startActivityForResult(
                        new Intent(
                                Intent.ACTION_PICK,
                                android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI
                        ),
                        204
                );
            }
        });

        videoo_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                startActivityForResult(
                        new Intent(
                                Intent.ACTION_PICK,
                                android.provider.MediaStore.Video.Media.INTERNAL_CONTENT_URI
                        ),
                        204
                );
            }
        });
    }
}
