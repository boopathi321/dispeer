package com.dispeer.app.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dispeer.app.adapter.ChatListAdapter;
import com.vweeter.dispeer.R;

/**
 * Created by My Windows on 8/20/2017.
 */

public class ChatsFragment extends BaseFragment {
    private RecyclerView recyclerView;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView  = inflater.inflate(R.layout.chats_layout, null);
        initControl(rootView);
        return rootView;
    }

    private void initControl(View view){
        recyclerView = (RecyclerView) view.findViewById(R.id.chatList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                LinearLayoutManager.VERTICAL);
        recyclerView.addItemDecoration(dividerItemDecoration);
        ChatListAdapter mChatListAdapter = new ChatListAdapter(getActivity());
        recyclerView.setAdapter(mChatListAdapter);
    }
}
