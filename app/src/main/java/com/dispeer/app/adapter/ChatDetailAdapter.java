package com.dispeer.app.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.vweeter.dispeer.R;

import java.util.ArrayList;

/**
 * Created by My Windows on 8/23/2017.
 */

public class ChatDetailAdapter extends RecyclerView.Adapter<ChatDetailAdapter.MyViewHolder> {

    private Context mContext;
    private ArrayList arrayList;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView rightBubble, leftBubble;
        RelativeLayout rightLayout, leftLayout;
        public MyViewHolder(View view) {
            super(view);
            rightBubble = (TextView) view.findViewById(R.id.rightBubble);
            leftBubble = (TextView) view.findViewById(R.id.leftBubble);
            rightLayout = (RelativeLayout) view.findViewById(R.id.right);
            leftLayout = (RelativeLayout) view.findViewById(R.id.left);
        }
    }


    public ChatDetailAdapter(Context mContext, ArrayList arrayList) {
        this.mContext = mContext;
        this.arrayList = arrayList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.chat_detail_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        if(position%2 == 0) {
            holder.rightBubble.setText(arrayList.get(position).toString());
            holder.rightLayout.setVisibility(View.VISIBLE);
            holder.leftLayout.setVisibility(View.GONE);
        }else{
            holder.leftBubble.setText(arrayList.get(position).toString());
            holder.leftLayout.setVisibility(View.VISIBLE);
            holder.rightLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }
}
