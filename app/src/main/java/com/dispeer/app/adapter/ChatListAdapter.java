package com.dispeer.app.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.dispeer.app.activity.ChatDetailActivity;
import com.dispeer.app.activity.DashBoardActivity;
import com.vweeter.dispeer.R;

/**
 * Created by My Windows on 8/22/2017.
 */


public class ChatListAdapter extends RecyclerView.Adapter<ChatListAdapter.MyViewHolder> {

    private Context mContext;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout chatItemParent;
        public MyViewHolder(View view) {
            super(view);
            chatItemParent = (RelativeLayout) view.findViewById(R.id.chatItemParent);
        }
    }


    public ChatListAdapter(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.chat_list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.chatItemParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(mContext, ChatDetailActivity.class);
                mContext.startActivity(mIntent, ActivityOptionsCompat.makeSceneTransitionAnimation((DashBoardActivity)mContext).toBundle());
            }
        });
    }

    @Override
    public int getItemCount() {
        return 3;
    }
}
