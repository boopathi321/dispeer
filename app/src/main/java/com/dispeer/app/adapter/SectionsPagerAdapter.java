package com.dispeer.app.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.dispeer.app.fragment.ChatsFragment;
import com.dispeer.app.fragment.ContactsFragment;
import com.dispeer.app.fragment.SettingsFragment;
import com.dispeer.app.fragment.TryLaterFragment;


public class SectionsPagerAdapter extends FragmentPagerAdapter {

    public SectionsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new ContactsFragment();
            case 1:
                return new ChatsFragment();
            case 2:
                return new SettingsFragment();
        }
        return new TryLaterFragment();
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "CONTACTS";
            case 1:
                return "CHATS";
            case 2:
                return "SETTINGS";
        }
        return null;
    }
}
