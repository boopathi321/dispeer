package com.dispeer.app.component;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.dispeer.app.activity.LoginActivity;
import com.vweeter.dispeer.R;

/**
 * Created by My Windows on 8/19/2017.
 */

public class BottomSheet extends BottomSheetDialogFragment {
    private Button btnClosePopup;
    private RelativeLayout hour,day,week,year,years;
    private LoginActivity mLoginActivity;
    @Override
    public void setupDialog(final Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        View layout = View.inflate(getContext(), R.layout.popup, null);
        btnClosePopup = (Button) layout.findViewById(R.id.cancel);
        hour=(RelativeLayout) layout.findViewById(R.id.validity_hour);
        day=(RelativeLayout) layout.findViewById(R.id.validity_day);
        week=(RelativeLayout) layout.findViewById(R.id.validity_week);
        year=(RelativeLayout) layout.findViewById(R.id.validity_year);
        years=(RelativeLayout) layout.findViewById(R.id.validity_Years);
        View.OnClickListener cancel_button_click_listener = new View.OnClickListener() {
            public void onClick(View v) {
                String code = "";
                if(v.getId() == hour.getId()){
                    code = "0";
                }else if(v.getId() == day.getId()){
                    code = "1";
                }else if(v.getId() == week.getId()){
                    code = "2";
                }else if(v.getId() == year.getId()){
                    code = "3";
                }else if(v.getId() == years.getId()){
                    code = "4";
                }
                if(mLoginActivity != null && !"".equals(code)){
                    mLoginActivity.registerUser(code);
                }
                dismiss();
            }
        };
        hour.setOnClickListener(cancel_button_click_listener);
        day.setOnClickListener(cancel_button_click_listener);
        week.setOnClickListener(cancel_button_click_listener);
        year.setOnClickListener(cancel_button_click_listener);
        years.setOnClickListener(cancel_button_click_listener);
        btnClosePopup.setOnClickListener(cancel_button_click_listener);
        dialog.setContentView(layout);

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) layout.getParent())
                .getLayoutParams();
        CoordinatorLayout.Behavior behavior = params.getBehavior();
        ((View) layout.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof LoginActivity){
            mLoginActivity = (LoginActivity)context;
        }
    }

    public interface BottomSheetListener{
        void registerUser(String code);
    }
}
